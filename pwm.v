`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/29/2021 04:57:00 AM
// Design Name: 
// Module Name: firmware
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module pwm(
  input CLK100MHZ,
  input [15:0] SW,
  input BTNC,
  input BTNR,
  
  output LED16_R,
  output LED16_G,
  output LED16_B,
  
  output [7:0] AN,
  output CA,
  output CB,
  output CC,
  output CD,
  output CE,
  output CF,
  output CG,
  
  output [15:0] LED
  );
  
  reg [15:0] freq;
  reg [15:0] pwm_max_step = 20000;
  reg [15:0] pwm_max_brightness = 10000;
  
  reg lgbt_b = 0, lgbt_r = 0, lgbt_g = 0;
  reg [15:0] pwm_b = 0, pwm_r = 0, pwm_g = 0;
  reg [2:0] mode = 0;
  
  assign LED16_R = lgbt_r;
  assign LED16_G = lgbt_g;
  assign LED16_B = lgbt_b;
  assign LED = pwm_r;
  
  always @(posedge CLK100MHZ) begin
    if(freq <= pwm_max_step) begin
        if(freq < pwm_r) lgbt_r = 1;
        else lgbt_r = 0;
        
        if(freq < pwm_g) lgbt_g = 1;
        else lgbt_g = 0;
        
        if(freq < pwm_b) lgbt_b = 1;
        else lgbt_b = 0;
    end else begin
        freq = 0;
        
        // rgb step
        case(mode)
          0: begin
            pwm_r = pwm_max_brightness; 
            pwm_g = 0; 
            pwm_b = pwm_b-1;
            if(pwm_b <= 0) begin
                pwm_b = 0; mode = 1;
            end
          end
          1: begin
            pwm_r = pwm_max_brightness; 
            pwm_g = pwm_g+1; 
            pwm_b = 0;
            if(pwm_g >= pwm_max_brightness) begin
                pwm_g = pwm_max_brightness; mode = 2;
            end
          end
          2: begin
            pwm_r = pwm_r-1; 
            pwm_g = pwm_max_brightness; 
            pwm_b = 0;
            if(pwm_r <= 0) begin
                pwm_r = 0; 
                mode = 3;
             end
           end
           3: begin
            pwm_r = 0; 
            pwm_g = pwm_max_brightness; 
            pwm_b = pwm_b + 1;
            if(pwm_b >= pwm_max_brightness) begin 
                pwm_b = pwm_max_brightness; mode = 4;
            end
           end
          4: begin
            pwm_r = 0; 
            pwm_g = pwm_g-1; 
            pwm_b = pwm_max_brightness;
            if(pwm_g <= 0) begin 
                pwm_g = 0; 
                mode = 5;
            end
           end
          5: begin
            pwm_r = pwm_r+1; 
            pwm_g = 0; 
            pwm_b = pwm_max_brightness;
            if(pwm_r >= pwm_max_brightness) begin 
                pwm_r = pwm_max_brightness; 
                mode = 0;
            end 
          end
        endcase    
    end
    
    freq = freq + 1;
  end
endmodule

