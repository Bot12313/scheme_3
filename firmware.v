`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/29/2021 04:57:00 AM
// Design Name: 
// Module Name: firmware
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module firmware(
  input CLK100MHZ,
  input [15:0] SW,
  input BTNC,
  input BTNR,
  
  output LED16_R,
  output LED16_G,
  output LED16_B,
  
  output [7:0] AN,
  output CA,
  output CB,
  output CC,
  output CD,
  output CE,
  output CF,
  output CG,
  
  output [15:0] LED
  );
  
  localparam SEGMENT_LATENCY = 20 * 1000 * 100; // 20 ms (50 FPS)
  
  wire [7:0] a;
  wire [7:0] b;
  assign a = SW[15:8];
  assign LED[15:8] = SW[15:8];
  assign b = SW[7:0];

  // Printing result to LEDs
  wire busy;
  wire [23:0] result;
  
  // Converting into BCD format
  reg [9:0] segment_clock = 0;
  reg [2:0] segment_num;
  assign AN = ~({8{~busy}} & (8'b0000_0001 << segment_num));
  
  wire [31:0] result_bcd;
  assign result_bcd[ 3: 0] = result              % 10;
  assign result_bcd[ 7: 4] = result /         10 % 10;
  assign result_bcd[11: 8] = result /        100 % 10;
  assign result_bcd[15:12] = result /      1_000 % 10;
  assign result_bcd[19:16] = result /     10_000 % 10;
  assign result_bcd[23:20] = result /    100_000 % 10;
  assign result_bcd[27:24] = result /  1_000_000 % 10;
  assign result_bcd[31:28] = result / 10_000_000 % 10;
  
  // Printing BCD on 7-segment display
  wire [3:0] num;
  wire print;
  assign num = result_bcd >> (segment_num * 4);
  assign print = segment_num == 0 || (result_bcd >> (segment_num * 4)) != 0;

  assign CA = ~print | (num == 1 || num == 4);
  assign CB = ~print | (num == 5 || num == 6);
  assign CC = ~print | (num == 2);
  assign CD = ~print | (num == 1 || num == 4 || num == 7);
  assign CE = ~print | (num == 1 || num == 3 || num == 4 || num == 5 || num == 7 || num == 9);
  assign CF = ~print | (num == 1 || num == 2 || num == 3 || num == 7);
  assign CG = ~print | (num == 0 || num == 1 || num == 7);

    //assign LED[15:12] = num;

  Main main1 (
    .clk_i(CLK100MHZ),
    .rst_i(BTNR),
    .start_i(BTNC),
    
    .a_bi(a),
    .b_bi(b),
    
    .busy_o(busy),
    .y_bo(result)  
  );
  
  assign LED[0] = busy;
  assign LED[7:5] = segment_num;
endmodule

