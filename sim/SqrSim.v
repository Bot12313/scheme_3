`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/29/2021 01:34:32 PM
// Design Name: 
// Module Name: SqrSim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SqrSim();

    reg clk;
    reg rst;
    reg [7:0] a;
    reg start;
    wire busy;
    wire [15:0] result,rescbrt;

    Multiply mult1 (
        .clk_i(clk),
        .rst_i(rst),
        .a_bi(a),
        .b_bi(a),
        .start_i(start),
        .busy_o(busy),
        .y_bo(result)
    );
    
    initial begin
        clk=1; rst=1;
        #1
        clk=0; rst=0;
        #1
        a=8'b0000_0001; clk=1; start=1;
        #1 clk=0; #1 clk=1; start=0;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        a=8'b0000_0010; clk=1; start=1;
        #1 clk=0; #1 clk=1; start=0;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        a=8'b0000_0100; clk=1; start=1;
        #1 clk=0; #1 clk=1; start=0;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        a=8'b0000_1000; clk=1; start=1;
        #1 clk=0; #1 clk=1; start=0;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        a=8'b0001_0000; clk=1; start=1;
        #1 clk=0; #1 clk=1; start=0;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        a=8'b0010_0000; clk=1; start=1;
        #1 clk=0; #1 clk=1; start=0;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        a=8'b0100_0000; clk=1; start=1;
        #1 clk=0; #1 clk=1; start=0;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        a=8'b1000_0000; clk=1; start=1;
        #1 clk=0; #1 clk=1; start=0;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
        #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1; #1 clk=0; #1 clk=1;
    end
endmodule